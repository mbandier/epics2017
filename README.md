This folder contains the following files/directories:

- EPDL2014.ALL.txt:        Contains the epics2014 EPDL library

- EPDL2017.ALL.txt:        Contains the epics2017 EPDL library (modified - with E instead of D, in respect with the
                                         original) - NB: it contains errors that are fixed in the EPDL2017.ALL-NEW.txt

- EPDL2017.ALL-NEW.txt:  New epics2017 data - after fixes from R. Cullen

- EADL/EADL2017.ALL.txt:          EADL2017 full dataset  - downloaded from https://www-nds.iaea.org/epics/libsall.htm the 28th March 2018

- EEDL/EEDL2017.ALL.txt:          EEDL2017 full dataset  - downloaded from https://www-nds.iaea.org/epics/libsall.htm the 28th March 2018

- G4LEDATA:                 Fake G4LEDATA dir containing /livermore/epics2017 folders - to be used to prepare data that will be put in Geant4 repository, once ready

- create-cs.C:                Macro to generate the pe-cs files    - small changes from Sebastien macro.
                                      Execute with     ROOT:
                                      root .x create-cs.C
                                      It reads EPDL2014.ALL.txt or EPDL2017.ALL.txt file, parse it and generate cs files. It expects
                                      to find phot2014 or phot2017 folders.

- create-scs.C:             Macro to generate the pe-ss-cs files - small changes from Sebastien macro. Execute with ROOT:
                                     root .x create-scs.C
                                     It reads EPDL2014.ALL.txt or EPDL2017.ALL.txt file, parse it and generate subshells cs files.
                                     It expects to find phot2014 or phot2017 folders.

- phot2014:                  Directory with the generated files from epics2014 - filtered for photoelectric effect

- phot2017:                  Directory with the generated files from epics2017 - filtered for photoelectric effect

- analyse.cc:                Simple program to analyse cs and subshell cs files from the two libraries and generate a file containing the
                             ratio of the cross-sections that correspond to the same energy value. NB: it expects to find the folders 
		             comparisonDenser2014-2017 and comparison2014-2017
                             Compile with:
                             g++ analyse.cc -o analyse -std=c++11
                             Execute with:
                             ./analyse <atomicNumberOfElement>

- simpleGnuplot:         Gnuplot script file fo generate graphs to compare epics2014 vs epics2017 cs data.
                                    Execute with "gnuplot simpleGnuplot" .
                                    It creates two images with the comparison of the two cross-sections and the corresponding
                                    ratio. To change element change the file. It expects to have data inside phot2014 and phot2017
                                    and to have run ./analyse on the element one wants to plot.

- multiplot and multiplot_ss:  Gnuplot script file to generate multiplots to compare cs and subshells cs files generated running ./analyse. 
			                       Run with: gnuplot -c multiplot <atomicNumber>
				               gnuplot -c multiplot_ss <atomicNumber> <idShell>
                                    
- create_phot_epics2017.C  ROOT macro to parse the total and partial cross-section files stored into phot2017
                                              folder and put them into a form similar to what done for epics2014, so that they can be
                                              loaded by the Geant4 photoelectric livermore model in the same way.
                                              Execute with:
                                              root .x create_phot_epics2017.C
                                              It expects to find the phot_epics2017 folder
- phot_epics2017                  Directory with files generated with create_phot_epics2017.C

- generateDenserDataset:    Program to generate a denser dataset (with more points) based on epics2014 original data.
                                             It needs a modified version of the photoelectric livermore model to run
- denserPhot2014:               Contains a copy of the data files created with generateDenserDataset
                                           
- fitPhotoElectricEffect:              ROOT macro to fit photoelectric effect subshells cross-sections. MODIFIED to
                                                   store non-cumulative fitting paramenters (necessary for GeantV vectorization)
                                                   
- samplingShells_photoeffect:   AliasSampling implementation for subshell sampling + Tests and plots

- visualizeSubShellsCS:             Macro to plot subshells cross-sections data that are under $G4LEDATA/livermore/epics2014/phot directory.
                                             

