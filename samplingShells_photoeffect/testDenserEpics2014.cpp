#include "AliasTable.h"
#include "Random.h"
#include "Hist.h"
#include "XSectionsVector.h"
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>


using namespace std;
using namespace geantphysics;
static XSectionsVector **fShellVector[100]; // Several subshell cross-section vector per Z
std::vector<int> shells;
static constexpr double gigaelectronvolt = 1.;
static constexpr double MeV = 1.e-3 *gigaelectronvolt;
static constexpr double centimeter  = 1.;
static constexpr double meter  = 100.*centimeter;
static constexpr double meter2 = meter*meter;
static constexpr double      barn = 1.e-28*meter2;
static constexpr double kElectronMassC2   = 0.510998910 * MeV;
static constexpr double gsingleTableErrorThreshold = 5.e-3; // 5 per mille error threshold
int fNumSamplingPrimEnergies;
int fNumAliasTables;
int fNumSamplingPrimEnergiesPerDecade= 20;
double fMaxPrimEnergy = 1; //1GeV
double fMinPrimEnergy = 1.e-7; //100eV
double fPrimEnLMin;
double fPrimEnILDelta;

double *fSamplingPrimEnergies;
double *fLSamplingPrimEnergies; // log of sampling gamma energies
geant::AliasTable *fAliasSampler;
//NB: it is important that the values given to the ALIAS table constructor are a PDF (the integral must be = 1)
double GenerateDiscretePDF(double x){
    return 1+x;
}

string sInt(int number)
{
    stringstream ss;//create a stringstream
    ss << number;//add number to the stream
    return ss.str();//return a string with the contents of the stream
}

string sDouble(double number)
{
    stringstream ss;//create a stringstream
    ss << number;//add number to the stream
    return ss.str();//return a string with the contents of the stream
}


//read /Users/mariba/GeantVproject/epics2017/phot2017-new/ subshells cross-sections files and put them into local pe-ss-cs files using the conventions (in the form energy | energy^3*CS)
void prepareData(std::vector<int>& shells){
    shells.push_back(1);
    shells.push_back(1);
    
    for(int Z=3 ; Z<99 ; Z++)
    {
        string s = sInt(Z);
        //std::string datafile = "/Users/mariba/GeantVproject/install/geant4.10.03.p01/share/Geant4-10.3.1/data/G4EMLOW6.50/livermore/epics2014/phot/pe-ss-cs-"+ s +".dat";

        //std::string datafile = "/Users/mariba/GeantVproject/epics2017/repository/denserPhot2014/pe-ss-cs-"+ s +".dat";
        std::string datafile = "/Users/mariba/GeantVproject/epics2017/repository/generateDenserDataset/build/denserEpics2014/pe-ss-cs-"+ s +".dat";
        string ffdata = (string)datafile;
        
        //IN
        ifstream  data_file(ffdata.c_str(), ios::in );
        if(data_file.fail()){
            cout << "Error opening data file"  << endl;
        }
        //else {
        //    cout << "data file " << datafile << " is opened" << endl;
        //}
        if(!data_file) {std::cout<<"ERROR!\n"; return;}
        //1. Subshell cross sections output file
        string outfile="phot_denserEpics_2014/pe-ss-cs-" + s + ".dat";
        
        ofstream fout(outfile.c_str(), ios::out);
        if(fout.fail()){
            cout << "Error opening output file: " << outfile << endl;
        }
        //else cout << "Output file <" << outfile << "> is opened" << endl;
        
        int n0, n1, n2=-1, n3, n4, n5;
        int nShells=0;
        double x, y, ek;
        std::vector<double> energy, xx, yy;
        
        //reading the file EPICS2014
        for (int N = 0; N < 99999; N++)
        {
            data_file >> x >> y;
            //end of file
            if(x < -1.5) {
                break;
            }
            else if(0.0 == y) {//first row of a new shell
                n0 = -1;    // number of low-energy points for one shell
                n1 = -1;    // number of points for fit for one shell
                n3 = 0;     // number of shells
                n2++;
                nShells++;
                energy.push_back(x); //ionization energy of shell "n2"
                xx.clear();
                yy.clear();
            }
            else if(x> 0.0){ //for all the other rows

                n0++; //counter of rows per shell
                xx.push_back(x);
                yy.push_back(y);

            }
            else{  //is the end of one shell (ROW -1 -1 reached) -- write infos in the file

                if(xx.size() > 0) {
                    fout << xx[0] << " " << xx[xx.size() - 1] << " " << xx.size()  << " " << n2
                    << endl;
                    //cout<<"Shell: "<<n2<<std::endl;
                    for(int i=0; i<xx.size() ; i++) {
                        fout << xx[i] << " " << xx[i]*xx[i]*xx[i]*yy[i] << endl;
                        //cout<<xx[i] << " " << xx[i]*xx[i]*xx[i]*yy[i] << endl;
                    }
                    //cout<<"********"<<std::endl;
                }

            }

        }
        std::cout<<"Total number of shells for Z:"<<Z<<" is: "<<nShells<<std::endl;
        shells.push_back(nShells);
    }
}
//read the local pe-ss-cs files and load the infos on fShellVector
void readData(int Z, int nShellsUsed){
    
    //    char *path = std::getenv("GEANT_PHYSICS_DATA");
    //    if (!path) {
    //        std::cerr<<"******   ERROR in SauterGavrilaPhotoElectricModel::ReadData() \n"
    //        <<"         GEANT_PHYSICS_DATA is not defined! Set the GEANT_PHYSICS_DATA\n"
    //        <<"         environment variable to the location of Geant data directory!\n"
    //        <<std::endl;
    //        exit(1);
    //    }
    
    fShellVector[Z] = new XSectionsVector*[nShellsUsed];
    for(int i=0; i<nShellsUsed; i++)
        fShellVector[Z][i]=new XSectionsVector;
    
    for (int i=0 ; i<nShellsUsed ; i++)
    {
        fShellVector[Z][i]->fDataVector.clear();
        fShellVector[Z][i]->fBinVector.clear();
        fShellVector[Z][i]->numberOfNodes=0;
    }
    
    //If more than one shell is used -> Read sub-shells cross section data
    if(1 < nShellsUsed) {
        std::ostringstream ost2;
        ost2 <<"pe-ss-cs-" << Z <<".dat";
        std::ifstream fin2(ost2.str().c_str());
        if( !fin2.is_open()) {
            std::cerr<< "SauterGavrilaPhotoElectricModel data file <" << ost2.str().c_str()
            << "> is not opened!" << std::endl;
            return;
        } else {
            
            int n3, n4;
            double x, y;
            
            for(int i=0; i<nShellsUsed; ++i)
            {
                fin2 >> x >> y >> n3 >> n4;
                //std::cout<<n3<<"  "<< n4<<std::endl;
                
                fShellVector[Z][i]->fBinVector.clear();
                fShellVector[Z][i]->fDataVector.clear();
                fShellVector[Z][i]->fBinVector.reserve(n3);
                fShellVector[Z][i]->fDataVector.reserve(n3);
                fShellVector[Z][i]->edgeMin=x*MeV;
                fShellVector[Z][i]->edgeMax=y*MeV;
                
                for(int j=0; j<n3; ++j)
                {
                    fin2 >> x >> y;
                    fShellVector[Z][i]->fBinVector.push_back(x*MeV);
                    fShellVector[Z][i]->fDataVector.push_back(y*barn);
                    fShellVector[Z][i]->numberOfNodes++;
                }
                fShellVector[Z][i]->fCompID=n4;
            }
            
            fin2.close();
        }
    }
    
}

struct LinAlias {
    /** @brief Number of data points i.e. size of the arrays =
     * SauterGavrilaPhotoElectricModel::fNumSamplingGammaEnergies. */
    int fNumdata;
    
    /** @brief Gamma discrete kinetic energies stored in the alias table -  (transformed or not..it depends). */
    double *fXdata;
    
    /** @brief The probability density function values (not necessarily normalised) over the photoelectron angle
     *        variable values. */
    double *fYdata;
    
    /** @brief The alias probabilities (not necessarily normalised) over the photoelectron angle
     *        variable values.
     */
    double *fAliasW;
    
    /** @brief The alias indices over the photon energy variable values. */
    int *fAliasIndx; // Alias indices
};

LinAlias **fAliasData; // alias data structure

double CalculatePartialCrossSection (int Z, int shell, double ekin){
    //std::cout<<"CalculatePartialCrossSection Z:"<<Z<<" shell: "<<shell<<std::endl;
    size_t idx=0;
    double cs=fShellVector[Z][shell]->GetValue(ekin, idx);
    //std::cout<<"---> cs:"<<cs<<std::endl;
    return cs;
}


int PrepareDiscreteAlias(int Z, double ekin, std::vector<double> & x, std::vector<double> & y){
    
    //std::cout<<"numShell Z:"<<Z<<" numShell: "<<shells[Z-1]<<std::endl;
    int numShell=shells[Z-1];
    x.resize(numShell);
    y.resize(numShell);
    //std::cout<<"numShell Z:"<<Z<<" numShell: "<<numShell<<std::endl;
    for (int i=0; i<numShell; i++){
        x[i]=i;
        y[i]= CalculatePartialCrossSection(Z, i, ekin);
    }
    
    return numShell;
}




void BuildOneDiscreteAlias(int Z, int indx, double ekin){
    
    std::vector<double> x;
    std::vector<double> y;
    //std::cout<<"PrepareDiscreteAlias for Z:"<<Z<<" indx: "<<indx<<std::endl;
    int numShell= PrepareDiscreteAlias(Z, ekin, x, y);
    
    //std::cout<<"bene: "<<indx<<std::endl;
    fAliasData[indx]              = new LinAlias();
    fAliasData[indx]->fNumdata    = numShell;
    fAliasData[indx]->fXdata      = new double[numShell];
    fAliasData[indx]->fYdata      = new double[numShell];
    fAliasData[indx]->fAliasW     = new double[numShell];
    fAliasData[indx]->fAliasIndx  = new    int[numShell];
    //copy data
    for (int i = 0; i < (int) x.size(); i++)
    {
        fAliasData[indx]->fXdata[i]=x[i];
        fAliasData[indx]->fYdata[i]=y[i];
    }
    
    // prepare the alias data for this PDF(x,y)
    fAliasSampler->PreparDiscreteTable(fAliasData[indx]->fYdata,fAliasData[indx]->fAliasW, fAliasData[indx]->fAliasIndx,fAliasData[indx]->fNumdata);
    
}
void testDiscreteSampling(){
    
    // create an AliasTable object
    geant::AliasTable *alias = new geant::AliasTable();
    
    //take Z=23
    int     numData = 10; // number of discrete values of the random variable
    double *x;
    double *y;
    double *w;
    int    *indx;
    
    // allocate space for the arrays
    x    = new double[numData]();
    y    = new double[numData]();
    w    = new double[numData]();
    indx = new int[numData]();
    
    // fill in x values and y values i.e. the p.d.f. at the x-values
    double xmin = 0;
    double xmax = 10.0;
    //double xdelta = (xmax-xmin)/(numData-1);
    
    FILE *f     = fopen("generatedPDF","w");
    double integral= 0.;
    //Generate the pdf and calculate the area
    for (int i=0; i<numData; ++i) {
        x[i] = i;
        y[i] = GenerateDiscretePDF(x[i]);
        integral+=y[i];
    }
    std::cout<<"GeneratedPDF:::\n";
    //Normalize the values over the integral to obtain the PDF
    for (int i=0; i<numData; ++i) {
        y[i] =y[i]/integral;
        fprintf(f,"%d\t%.8g\t%.8g\n",i,x[i],y[i]);
        std::cout<< i<<"\t"<< x[i] << "\t"<<y[i] <<std::endl;
    }
    fclose(f);
    
    
    // prepare the alias sampler for linear approximation of the p.d.f. within the bins
    alias->PreparDiscreteTable(y,w,indx,numData);
    std::cout<<"Debug of the alias table::::::\n";
    for(int i=0; i<numData; i++)
        std::cout<<x[i]<<"\t"<<y[i]<<"\t"<<w[i]<<"\t"<<indx[i]<<std::endl;
    
    // set number of required samples
    long int numSample =1.e08;
    // create the histogram object
    geant::Hist *histo = new geant::Hist(xmin,xmax,10);
    std::cout<<"============================\n";
    
    
    double weight = 1.0/numSample;
    for (long int i=0; i<numSample; ++i) {
        double r1 = geant::Random::UniformRand();
        double r2 = geant::Random::UniformRand();
        // get one sample
        double xsampl = alias->SampleDiscrete(w,indx,numData,r1);
        //double xsampl = alias->SampleLinear(x,y,w,indx,numData,r1, r2);
        // fill the histogram
        histo->Fill(xsampl,weight);
    }
    FILE *f2     = fopen("sampledPDF","w");
    // print out histogram in normalized form
    std::cout<<"Sampled values:::\n";
    for (int i=0; i<histo->GetNumBins(); ++i) {
        //std::cout<<histo->GetX()[i]+0.5*histo->GetDelta()<<"  "<<histo->GetY()[i]/histo->GetDelta()<<std::endl;
        //fprintf(f,"%d\t%.8g\t%.8g\n",i,histo->GetX()[i]+0.5*histo->GetDelta(),histo->GetY()[i]/histo->GetDelta());
        fprintf(f2,"%d\t%.8g\t%.8g\n",i,histo->GetX()[i],histo->GetY()[i]/histo->GetDelta());
        std::cout<< histo->GetX()[i] << "\t"<<histo->GetY()[i]/histo->GetDelta()<<std::endl;
    }
    fclose(f2);
    
    //
    // delete all dynamically allocated objects
    delete alias;
    delete histo;
    delete [] x;
    delete [] y;
    delete [] w;
    delete [] indx;
}

void InitSamplingTables(int* Z, int numElements) {
    
    // set number of primary gamma energy grid points
    // keep the prev. value of primary energy grid points.
    
    int oldNumAliasTables = fNumAliasTables;
    int oldNumGridPoints = fNumSamplingPrimEnergies;
    fNumSamplingPrimEnergies = fNumSamplingPrimEnergiesPerDecade*std::lrint(std::log10(fMaxPrimEnergy/fMinPrimEnergy))+1;
    if (fNumSamplingPrimEnergies<2) {
        fNumSamplingPrimEnergies = 2;
    }
    fNumAliasTables = numElements*fNumSamplingPrimEnergies;
    
    // set up the initial gamma energy grid
    if (fSamplingPrimEnergies) {
        delete [] fSamplingPrimEnergies;
        delete [] fLSamplingPrimEnergies;
        fSamplingPrimEnergies  = nullptr;
        fLSamplingPrimEnergies = nullptr;
    }
    
    fSamplingPrimEnergies  = new double[fNumSamplingPrimEnergies];
    fLSamplingPrimEnergies = new double[fNumSamplingPrimEnergies];
    
    fPrimEnLMin    = std::log(fMinPrimEnergy);
    double delta   = std::log(fMaxPrimEnergy/fMinPrimEnergy)/(fNumSamplingPrimEnergies-1.0);
    fPrimEnILDelta = 1.0/delta;
    fSamplingPrimEnergies[0]  = fMinPrimEnergy;
    fLSamplingPrimEnergies[0] = fPrimEnLMin;
    fSamplingPrimEnergies[fNumSamplingPrimEnergies-1]  = fMaxPrimEnergy;
    fLSamplingPrimEnergies[fNumSamplingPrimEnergies-1] = std::log(fMaxPrimEnergy);
    for (int i=1; i<fNumSamplingPrimEnergies-1; ++i) {
        fLSamplingPrimEnergies[i] = fPrimEnLMin+i*delta;
        fSamplingPrimEnergies[i]  = std::exp(fPrimEnLMin+i*delta);
    }
    //
    // build the sampling tables at each primary gamma energy grid point.
    //
    // prepare the array that stores pointers to sampling data structures
    if (fAliasData) {
        for (int i=0; i<oldNumGridPoints; ++i) {
            if (fAliasData[i]) {
                delete [] fAliasData[i]->fXdata;
                delete [] fAliasData[i]->fYdata;
                delete [] fAliasData[i]->fAliasW;
                delete [] fAliasData[i]->fAliasIndx;
                delete fAliasData[i];
            }
        }
        delete [] fAliasData;
    }
    // create new fAliasData array
//    fAliasData = new LinAlias*[fNumSamplingPrimEnergies];
    fAliasData = new LinAlias*[fNumAliasTables];
    for (int i=0; i<fNumAliasTables; ++i) {
        fAliasData[i] = nullptr;
    }
    // create one sampling data structure at each primary gamma energy grid point:
    // -first create an AliasTable object
    if (fAliasSampler) {
        delete fAliasSampler;
    }
    // -the prepare each table one-by-one ///FOR THE MOMENT ONLY TABLES FOR ONE Z --> change the index
    for (int i=3; i<numElements; ++i) {
        //std::cout<<"numElements: "<<numElements<<std::endl;
        for(int j=0; j<fNumSamplingPrimEnergies; j++)
        {
            //std::cout<<"fNumSamplingPrimEnergies: "<<fNumSamplingPrimEnergies<<std::endl;
            int localIndex=i*fNumSamplingPrimEnergies+j;
            //std::cout<<"BuildOneDiscreteAlias #index:"<<localIndex<<" for element Z["<<i<<"]: "<<Z[i]<<" @energy: "<<fSamplingPrimEnergies[j]<<std::endl;
            
            BuildOneDiscreteAlias(Z[i], i*fNumSamplingPrimEnergies+j, fSamplingPrimEnergies[j]);
            
        }
        //exit(-1);
        
    }
}


void Test1(int* zeds){
    
    for (int i=3; i<99; i++)
    {
        
        //test only Z=82
        int Z= zeds[i];
        
        // set number of required samples
        long int numSample =1.e7;
        // create the histogram object
        int numShells=shells[Z-1];
        
        std::cout<<"============================\n";
        //std::cout<<"Processing Element: "<<zeds[i]<<" with number of shells: "<<numShells<<std::endl;
        
        double *x;
        double *y;
        
        // allocate space for the arrays
        x    = new double[numShells]();
        y    = new double[numShells]();
        //std::cout<<"fNumSamplingPrimEnergies:::"<<fNumSamplingPrimEnergies<<"\n";
        double maxError=0;
        int counter=0;
        
        for (int enIndex=0; enIndex<fNumSamplingPrimEnergies-1; enIndex++)
        {
            geant::Hist *histo = new geant::Hist(0,numShells,numShells);
            ///GENERATE
            //FILE *f     = fopen("generatedPDF","w");
            double integral= 0.;
            double ekin=(fSamplingPrimEnergies[enIndex]+fSamplingPrimEnergies[enIndex+1])/2;//(fSamplingPrimEnergies[1]+fSamplingPrimEnergies[0])/2;
            std::cout<<"Testing  element "<<Z<<" at "<<fSamplingPrimEnergies[enIndex]*1000<<" MeV"<<std::endl;
            //Generate the pdf and calculate the area
            for (int k=0; k<numShells; ++k) {
                x[k] = k;
                y[k] = CalculatePartialCrossSection(Z, k, ekin);
                integral+=y[k];
            }
            //std::cout<<"GeneratedPDF:::\n";
            //Normalize the values over the integral to obtain the PDF
            for (int i=0; i<numShells; ++i) {
                y[i] =y[i]/integral;
                //            fprintf(f,"%d\t%.8g\t%.8g\n",i,x[i],y[i]);
                //            std::cout<< i<<"\t"<< x[i] << "\t"<<y[i] <<std::endl;
            }
            //fclose(f);
            
            double weight = 1.0/numSample;
            
            
            for (long int i=0; i<numSample; ++i) {
                double r1 = geant::Random::UniformRand();
                double r2 = geant::Random::UniformRand();
                
                //// sample of the table
                double lGammaEnergy  = std::log(ekin);
                int tableIndex  = (int) ((lGammaEnergy-fPrimEnLMin)*fPrimEnILDelta);
                
                //
                if (tableIndex>=fNumSamplingPrimEnergies-1)
                    tableIndex = fNumSamplingPrimEnergies-2;
                //
                //std::cout<<"::::Ancor::::"<<tableIndex<<"\n";
                //std::cout<<"::::fLSamplingPrimEnergies[gammaEnergyIndx+1]::::"<<fLSamplingPrimEnergies[tableIndex+1]<<"\n";
                double pLowerGammaEner = (fLSamplingPrimEnergies[tableIndex+1]-lGammaEnergy)*fPrimEnILDelta;
                if (r1>pLowerGammaEner) {
                    ++tableIndex;
                }
                //this has to be tranformed to the localIndex, considering the Z
                int indx=Z*fNumSamplingPrimEnergies+tableIndex;
                //std::cout<<"Sampling from table index: "<<indx<<std::endl;
                //exit(-1);
                
                //int indx=tableIndex;
                int xsampl = fAliasSampler->SampleDiscrete(fAliasData[indx]->fAliasW, fAliasData[indx]->fAliasIndx, fAliasData[indx]->fNumdata, r2);
                // fill the histogram
                //check on ionization energies!
                //if (ekin > fAliasData[indx]->edgeMin)
                histo->Fill(xsampl,weight);
            }
            //        FILE *f2     = fopen("sampledPDF","w");
            //        // print out histogram in normalized form
            //        std::cout<<"Sampled values:::\n";
            //        for (int i=0; i<histo->GetNumBins(); ++i) {
            //
            //            fprintf(f2,"%d\t%.8g\t%.8g\n",i,histo->GetX()[i],histo->GetY()[i]/histo->GetDelta());
            //            std::cout<< histo->GetX()[i] << "\t"<<histo->GetY()[i]/histo->GetDelta()<<std::endl;
            //        }
            //        fclose(f2);
            //
            //        FILE *f3     = fopen("ratioPDF","w");
            for (int i=0; i<numShells; ++i){
                double ratio =y[i]/(histo->GetY()[i]/histo->GetDelta());
                //fprintf(f3,"%d\t%.8g\t%.8g\n",i,x[i],ratio);
                double error=std::abs(1-ratio);
                if(error>gsingleTableErrorThreshold && y[i]>1.e-5 && i==0)
                {
                    counter++;
                    if(error> maxError) maxError=error;
                    std::cout<<"ERROR GREATER THAN THRESHOLD: "<<error<<" for Energy: "<<ekin*1000<<" MeV  and shell"<< i <<"!!\n";
                    //exit(-1);
                }
            }
            //fclose(f3);
            
            //std::cout<<"Sampled at: "<<ekin*1000<" MeV"<<std::endl;
            
            delete histo;
            
        }
        
        std::cout<<"Summary for Z="<<Z<<": maxError:  "<<maxError<<"\t"<<counter<<" times over the threshold!"<<std::endl;
        
        delete [] x;
        delete [] y;
        
    }
    
}


void Test2(int Z, int numShells)
{
   
    // set number of required samples
    long int numSample =1.e7;
    std::cout<<"============================\n";
    std::cout<<"Testing Element: "<<Z<<" with number of shells: "<<numShells<<std::endl;
    
    double *x;
    double *y;
        
    // allocate space for the arrays
    x    = new double[numShells]();
    y    = new double[numShells]();
    //std::cout<<"fNumSamplingPrimEnergies:::"<<fNumSamplingPrimEnergies<<"\n";
    double maxError=0;
    int counter=0;
        
    for (int enIndex=0; enIndex<fNumSamplingPrimEnergies-1; enIndex++){
        double delta = (fSamplingPrimEnergies[enIndex+1] - fSamplingPrimEnergies[enIndex])/100;
        for (int energySpan=0; energySpan<100; energySpan++){
            
            geant::Hist *histo = new geant::Hist(0,numShells,numShells);
            ///GENERATE
            std::string nameFile= "resultsTest/generatedPDF_"+std::to_string(enIndex)+ "_"+std::to_string(energySpan)+".txt";
            const char* name1=nameFile.c_str();
            FILE *f     = fopen(name1,"w");
            double integral= 0.;
            double ekin=fSamplingPrimEnergies[enIndex] + energySpan*delta;
            std::cout<<"Testing  element "<<Z<<" at "<<fSamplingPrimEnergies[enIndex]*1000<<" MeV"<<std::endl;
            //Generate the pdf and calculate the area
            for (int k=0; k<numShells; ++k) {
                x[k] = k;
                y[k] = CalculatePartialCrossSection(Z, k, ekin);
                integral+=y[k];
            }
            //std::cout<<"GeneratedPDF:::\n";
            //Normalize the values over the integral to obtain the PDF
            for (int i=0; i<numShells; ++i) {
                y[i] =y[i]/integral;
                            fprintf(f,"%d\t%.8g\t%.8g\n",i,x[i],y[i]);
                            std::cout<< i<<"\t"<< x[i] << "\t"<<y[i] <<std::endl;
            }
            fclose(f);
        
            double weight = 1.0/numSample;
            
            
            for (long int i=0; i<numSample; ++i) {
                double r1 = geant::Random::UniformRand();
                double r2 = geant::Random::UniformRand();
                
                //// sample of the table
                double lGammaEnergy  = std::log(ekin);
                int tableIndex  = (int) ((lGammaEnergy-fPrimEnLMin)*fPrimEnILDelta);
                
                //
                if (tableIndex>=fNumSamplingPrimEnergies-1)
                    tableIndex = fNumSamplingPrimEnergies-2;
                //
                //std::cout<<"::::Ancor::::"<<tableIndex<<"\n";
                //std::cout<<"::::fLSamplingPrimEnergies[gammaEnergyIndx+1]::::"<<fLSamplingPrimEnergies[tableIndex+1]<<"\n";
                double pLowerGammaEner = (fLSamplingPrimEnergies[tableIndex+1]-lGammaEnergy)*fPrimEnILDelta;
                if (r1>pLowerGammaEner) {
                    ++tableIndex;
                }
                //this has to be tranformed to the localIndex, considering the Z
                int indx=Z*fNumSamplingPrimEnergies+tableIndex;
                //std::cout<<"Sampling from table index: "<<indx<<std::endl;
                //exit(-1);
                
                //int indx=tableIndex;
                int xsampl = fAliasSampler->SampleDiscrete(fAliasData[indx]->fAliasW, fAliasData[indx]->fAliasIndx, fAliasData[indx]->fNumdata, r2);
                // fill the histogram
                //check on ionization energies!
                //if (ekin > fAliasData[indx]->edgeMin)
                histo->Fill(xsampl,weight);
            }
            std::string nameFile2= "resultsTest/sampledPDF_"+std::to_string(enIndex)+ "_"+std::to_string(energySpan)+".txt";
            const char* name2= nameFile2.c_str();
            FILE *f2     = fopen(name2,"w");
            // print out histogram in normalized form
            std::cout<<"Sampled values:::\n";
            for (int i=0; i<histo->GetNumBins(); ++i) {
                fprintf(f2,"%d\t%.8g\t%.8g\n",i,histo->GetX()[i],histo->GetY()[i]/histo->GetDelta());
                std::cout<< histo->GetX()[i] << "\t"<<histo->GetY()[i]/histo->GetDelta()<<std::endl;
                
            }
            fclose(f2);
            std::string nameFile3= "resultsTest/ratioPDF_"+std::to_string(enIndex)+ "_"+std::to_string(energySpan)+".txt";
            const char* name3=nameFile3.c_str();
            FILE *f3     = fopen(name3,"w");
            for (int i=0; i<numShells; ++i){
                double ratio =y[i]/(histo->GetY()[i]/histo->GetDelta());
                fprintf(f3,"%d\t%.8g\t%.8g\n",i,x[i],ratio);
                double error=std::abs(1-ratio);
                if(error>gsingleTableErrorThreshold && y[i]>1.e-5 && i==0)
                {
                    counter++;
                    if(error> maxError) maxError=error;
                    std::cout<<"ERROR GREATER THAN THRESHOLD: "<<error<<" for Energy: "<<ekin*1000<<" MeV  and shell"<< i <<"!!\n";
                    //exit(-1);
                }
            }
            fclose(f3);
            //std::cout<<"Sampled at: "<<ekin*1000<" MeV"<<std::endl;
            delete histo;
        }

        
    }
    delete [] x;
    delete [] y;
    std::cout<<"Summary for Z="<<Z<<": maxError:  "<<maxError<<"\t"<<counter<<" times over the threshold!"<<std::endl;
}

int main(){
    
    
    prepareData(shells);
    //exit(-1);
    //shells.resize(100);
    //shells[3]=2;
    //for (int i=0; i<shells.size(); i++)
    //    std::cout<<"Shells Z: "<<i+1<<": "<<shells[i]<<std::endl;
    
    int zeds[99];
    
    for(int i=1; i<=shells.size(); i++)
    {
        std::cout<<"Read DATA and shells of Z = "<<i<<": "<<shells[i-1]<<std::endl;
        readData(i, shells[i-1]);
        zeds[i]=i;
    }
    std::cout<<"shells.size(): "<<shells.size()<<std::endl;
    //exit(-1);
    
    std::cout<<"Inizializing sampling tables\n";
    //exit(-1);
    InitSamplingTables(zeds,99);
    std::cout<<"Done\n";
    
    //Test1(zeds);
    //Test all the Z to print the maximum error
    
    Test2(zeds[82], shells[81]);
    
    return 0;
}
