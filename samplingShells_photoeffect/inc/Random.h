
#ifndef RANDOM_H
#define RANDOM_H

#include <cstdlib>
#include <random>

// IT IS JUST FOR TESTING
namespace geant {

class Random{
public:
   static std::mt19937 generator;
   static std::uniform_real_distribution<double> dis;
public:

  static double UniformRand(){
    //return ((double)generator())/std::mt19937::max();
    return dis(generator);
    //return ((double)std::rand())/RAND_MAX;
  }



};
}
#endif
