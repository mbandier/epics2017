#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

void analyseSScS (const char *  Z){
    
    std::ostringstream ost1;
    ost1 << "phot2017/pe-ss-cs-" << Z <<".dat";
    std::ostringstream ost2;
    ost2 << "phot2014/pe-ss-cs-" << Z <<".dat";
    //ost2 << "denserPhot2014/pe-ss-cs-" << Z <<".dat";
    FILE *f1 = fopen(ost1.str().c_str(), "r");
    FILE *f2 = fopen(ost2.str().c_str(), "r");
    double x, y, ek;
    std::vector<double> energy[26], xx[26], yy[26];
    std::vector<double> energy2[26], xx2[26], yy2[26];
    int ncols;
    int shellId1=-1;
    //reading f1
    for (int N = 0; N < 99999; N++)
    {
        ncols =  fscanf(f1,"%lf %lf",&x,&y);
        //end of file
        if(x < -1.5) {
            break;
        }
        else if(0.0 == y) {
            //it means there is a new shell
            shellId1++;
            //energy.push_back(x);//binding energy
            //xx[shell].clear();
            //yy[shell].clear();
            
        }
        else if(x> 0.0){
            
            xx[shellId1].push_back(x);
            yy[shellId1].push_back(y);
            
        }
        
    }
    int shellId2=0;
    //reading f2
    for (int N = 0; N < 99999; N++)
    {

        ncols =  fscanf(f2,"%lf %lf",&x,&y);
        //end of file
        //std::cout<<"N :"<<N<<"\n";
        if(x < -1.5) {
            break;
            //std::cout<<"Breaking N: "<<N<<"\n";
        }
        else if(-1 == x) {
            //it means there is a new shell
            shellId2++;
            //energy.push_back(x);//binding energy
            //xx[shell].clear();
            //yy[shell].clear();
        }
        else if(x> 0.0){
            //std::cout<<"shellId2: "<<shellId2<<std::endl;
            //std::cout<<"shellId2: "<<shellId2<<std::endl;
            xx2[shellId2].push_back(x);
            yy2[shellId2].push_back(y);
        }

        
    }
    //std::cout<<"Nshells1: "<<shellId1<<" Nshells2: "<<shellId2<<std::endl;
    
    for(int shellIdx=0; shellIdx<shellId1; shellIdx++)
    {
        std::ostringstream ost3;
        ost3 << "comparison2014-2017/div-pe-ss-cs-" << Z <<"-shell"<<shellIdx<<".dat";
        //ost3 << "comparisonDenser2014-2017/div-pe-ss-cs-" << Z <<"-shell"<<shellIdx<<".dat";
        std::ostringstream ost4;
        ost4 << "phot2014/pe-ss-cs-" << Z <<"-shell"<<shellIdx<<".dat";
        //ost4 << "denserPhot2014/pe-ss-cs-" << Z <<"-shell"<<shellIdx<<".dat";
        std::ostringstream ost5;
        ost5 << "phot2017/pe-ss-cs-" << Z <<"-shell"<<shellIdx<<".dat";
        FILE *f3 = fopen(ost3.str().c_str(), "w");
        FILE *f4 = fopen(ost4.str().c_str(), "w");
        FILE *f5 = fopen(ost5.str().c_str(), "w");
        int counterEn=0, counterXS=0;
   
        for(int i=0; i<xx[shellIdx].size(); i++)
        {
            fprintf(f5,"%lf %lf\n",xx[shellIdx][i], yy[shellIdx][i]);
            for(int j=0; j<xx2[shellIdx].size(); j++)
            {
                if(i==0) fprintf(f4,"%lf %lf\n",xx2[shellIdx][j], yy2[shellIdx][j]);
                if(xx[shellIdx][i]==xx2[shellIdx][j]){
                    counterEn++;
                    if(yy2[shellIdx][j]!=0){
                        fprintf(f3,"%lf %lf\n",xx[shellIdx][i], yy[shellIdx][i]/yy2[shellIdx][j]);
                    }
                    
                    if(yy[shellIdx][i]==yy2[shellIdx][j]) counterXS++;
                }
            }
        }
        cout<<"*** Summary subshells cs phot2014-phot2017  ***"<<endl;
        //cout<<"*** Summary subshells cs denser_phot2014-phot2017  ***"<<endl;
        cout<<"Total n. points epics2017 for shell "<<shellIdx<<": "<<xx[shellIdx].size()<<endl;
        cout<<"Total n. points epics2014 for shell "<<shellIdx<<": "<<xx2[shellIdx].size()<<endl;
        cout<<"Total n. equivalent energy points: "<<counterEn<<endl;
        cout<<"Total n. equivalent xsections: "<<counterXS<<endl;
        fclose(f3);
        fclose(f4);
        fclose(f5);
    }
    
    fclose(f1);
    fclose(f2);
    
    
    
    
}


void analyseCS(const char *  Z ){
    
    std::ostringstream ost1;
    ost1 << "phot2017/pe-cs-" << Z <<".dat";
    std::ostringstream ost2;
    ost2 << "phot2014/pe-cs-" << Z <<".dat";
    //ost2 << "pe-cs-" << Z <<".dat";
    
    FILE *f1 = fopen(ost1.str().c_str(), "r");
    FILE *f2 = fopen(ost2.str().c_str(), "r");
    
    double x, y, ek;
    std::vector<double> energy, xx, yy;
    std::vector<double> energy2, xx2, yy2;
    
    int ncols;
    
    //reading f1
    for (int N = 0; N < 99999; N++)
    {
        ncols =  fscanf(f1,"%lf %lf",&x,&y);
        //end of file
        if(x < -1.5) {
            break;
        }
        else if(0.0 == y) {
            energy.push_back(x);
            xx.clear();
            yy.clear();
        }
        else if(x> 0.0){
            
            xx.push_back(x);
            yy.push_back(y);
            
        }
    }
    
    //reading f2
    for (int N = 0; N < 99999; N++)
    {
        ncols =  fscanf(f2,"%lf %lf",&x,&y);
        if(x < 0.) {
            break;
        }
        else if(0.0 == y) {
            energy2.push_back(x);
            xx2.clear();
            yy2.clear();
        }
        else if(x> 0.0){
            xx2.push_back(x);
            yy2.push_back(y);
            
        }
    }
    
    std::ostringstream ost3;
    ost3 << "comparison2014-2017/div-pe-cs-" << Z <<".dat";
    FILE *f3 = fopen(ost3.str().c_str(), "w");
    int counterEn=0, counterXS=0;
    for(int i=0; i<xx.size(); i++)
    {
        for(int j=0; j<xx2.size(); j++)
        {
            if(xx[i]==xx2[j]){
                counterEn++;
                fprintf(f3,"%lf %lf\n",xx[i],yy[i]/yy2[j]);
                if(yy[i]==yy2[j]) counterXS++;
            }
        }
        
        
    }
    cout<<"*** Summary comparison cs epics2014-epics2017 ***"<<endl;
    cout<<"Total n. points epics2017: "<<xx.size()<<endl;
    cout<<"Total n. points epics2014: "<<xx2.size()<<endl;
    cout<<"Total n. equivalent energy points: "<<counterEn<<endl;
    cout<<"Total n. equivalent xsections: "<<counterXS<<endl;
    
    
    fclose(f1);
    fclose(f2);
    
}

int main(int argc, char** argv)
{
    if(argc<2){
        std::cout<<"Usage "<<argv[0]<<" <atomicNumber> "<<std::endl;
        return -1;
    }
    const char *  Z = argv[1];
    analyseCS(Z);
    analyseSScS(Z);
    
    
    
}
