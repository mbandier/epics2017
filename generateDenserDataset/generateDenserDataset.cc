//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
//
// -------------------------------------------------------------------
//      GEANT 4 class file --- Copyright CERN 1998
//      CERN Geneva Switzerland
//
//      Author:       : M. Bandieramonte
// -------------------------------------------------------------------

#include "globals.hh"
#include "G4ios.hh"
#include <fstream>
#include <iomanip>

#include "G4Material.hh"
#include "G4ElementVector.hh"
#include "G4ProcessManager.hh"
#include "G4VParticleChange.hh"
#include "G4ParticleChange.hh"

#include "G4ParticleTable.hh"
#include "G4DynamicParticle.hh"
#include "G4Electron.hh"
#include "G4Positron.hh"
#include "G4Gamma.hh"

#include "G4ForceCondition.hh"
#include "G4Box.hh"
#include "G4PVPlacement.hh"
#include "G4Step.hh"
#include "G4GRSVolume.hh"
#include "G4Region.hh"

#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

#include "G4StateManager.hh"
#include "G4NistManager.hh"

#include "Histo.hh"
#include "G4Timer.hh"

#include "G4TouchableHistory.hh"

#include "G4LivermorePhotoElectricModel.hh"
#include "G4LivermorePhotoElectricModelOLD.hh"
#include "G4LivermorePhotoElectricModel_new.hh"
#include "G4LivermorePolarizedPhotoElectricModel.hh"
#include "G4PenelopePhotoElectricModel.hh"
#include "G4PEEffectFluoModel.hh"

#include "G4ParticleChangeForGamma.hh"
#include "G4UAtomicDeexcitation.hh"
#include "G4LossTableManager.hh"


#include "G4UImanager.hh"

std::string sDouble(double number)
{
    std::stringstream ss;
    ss << number;
    return ss.str();
}

int main(int argc, char** argv)
{
    G4UImanager::GetUIpointer();
    G4cout << "========================================================" << G4endl;
    G4cout << "======        Generate Denser dataset           ========" << G4endl;
    G4cout << "========================================================" << G4endl;
    //-----------------------------------------------------------------------------
    // ------- Initialisation
    
    G4int     verbose  = 0;
    G4int     stat     = 1000000;
    G4int     Z = 8;
    G4double  energy   = 10.*MeV;
    G4Material* mat = 0;
    
    // convert string to Z
    G4String sz = "";
    if(argc >= 2) {
        sz = argv[1];
        std::istringstream is(sz);
        is >> Z;
    }
    
    // convert string to energy
    G4String se = "";
    if(argc >= 3) {
        se = argv[2];
        std::istringstream is(se);
        is >> energy;
        energy *= MeV;
    }
    
    // convert string to statistics
    G4String ss = "";
    if(argc >= 4) {
        ss = argv[3];
        std::istringstream is(ss);
        is >> stat;
    }
    
    // -------- Material
    G4NistManager::Instance()->SetVerbose(2);
    G4String mname = "G4_";
    if(Z < 99) {
        const G4Element* elm = G4NistManager::Instance()->FindOrBuildElement(Z);
        mname += elm->GetName();
    } else {
        mname += "WATER";
    }
    mat = G4NistManager::Instance()->FindOrBuildMaterial(mname);
    //G4NistManager::Instance()->FindOrBuildMaterial("G4_WATER");
    
    G4MaterialCutsCouple* couple = new G4MaterialCutsCouple(mat);
    couple->SetIndex(0);
    
    //G4double coeff = mat->GetDensity()*cm2/g;
    //G4double coeff = 1;
    
    if(!mat) { exit(1); }

    G4cout << "E(MeV)= " << energy/MeV << "  Z= " << Z
    << "  " << mat->GetName() << G4endl;
    
    // Track
    G4ThreeVector aPosition = G4ThreeVector(0.,0.,0.);
    G4ThreeVector aDirection = G4ThreeVector(0.0,0.0,1.0);
    const G4ParticleDefinition* part = G4Gamma::Gamma();
    G4Electron::Electron();
    G4Positron::Positron();
    
    G4DynamicParticle dParticle(part,aDirection,energy);
    dParticle.SetPolarization(0.0,1.0,0.0);
    
    G4cout.setf( std::ios::scientific, std::ios::floatfield );
    
    G4ParticleTable* partTable = G4ParticleTable::GetParticleTable();
    partTable->SetReadiness();
    
    //--------- Geometry definition
    
    G4double dimX = 100.0*cm;
    G4double dimY = 100.0*cm;
    G4double dimZ = 100.0*cm;
    
    G4Box* sFrame = new G4Box ("Box",dimX, dimY, dimZ);
    G4LogicalVolume* lFrame = new G4LogicalVolume(sFrame,mat,"Box",0,0,0);
    G4PVPlacement* pFrame = new G4PVPlacement(0,G4ThreeVector(),"Box",
                                              lFrame,0,false,0);
    
    G4ProductionCutsTable* theCoupleTable =
    G4ProductionCutsTable::GetProductionCutsTable();
    theCoupleTable->UpdateCoupleTable(pFrame);
    
    G4Region* reg = new G4Region("DefaultRegionForTheWorld");
    reg->AddRootLogicalVolume(lFrame);
    
    // -------------------------------------------------------------------
    // -------- Start run processing
    
    G4StateManager* g4State=G4StateManager::GetStateManager();
    if (! g4State->SetNewState(G4State_Init)) {
        G4cout << "error changing G4state"<< G4endl;;
    }
    
    //commented out to run without deexcitation
    G4VAtomDeexcitation* de = new G4UAtomicDeexcitation();
    G4LossTableManager::Instance()->SetAtomDeexcitation(de);
    de->SetFluo(true);
    de->InitialiseAtomicDeexcitation();
    de->SetFluo(false);
    
    // Instantiate model
    G4LivermorePhotoElectricModel_new livNEW;
    G4LivermorePhotoElectricModel liv;

    // Set particle change object
    G4ParticleChangeForGamma* fParticleChange = new G4ParticleChangeForGamma();
    livNEW.SetParticleChange(fParticleChange, 0);
    liv.SetParticleChange(fParticleChange, 0);
    G4DataVector cuts;
    cuts.push_back(keV);
    
    // Initilise model
    livNEW.Initialise(part, cuts);
    livNEW.SetCurrentCouple(couple);
    liv.Initialise(part, cuts);
    liv.SetCurrentCouple(couple);

    // -------- Track
    G4Track* track = new G4Track(&dParticle,0.0,aPosition);
    G4TouchableHandle fpTouchable(new G4TouchableHistory());
    track->SetTouchableHandle(fpTouchable);
    
    // -------- Step
    if(!G4StateManager::GetStateManager()->SetNewState(G4State_Idle))
    { G4cout << "G4StateManager PROBLEM! " << G4endl; }
    
    double emin;//1*eV;
    double emax=100*GeV;
    
    int numShells= livNEW.GetNumShells(Z);
    
    //************ READ THE SS CS FILE I WANT TO COMPARE WITH
    int nshells=0;
    std::ostringstream ost2;
    double x,y;
    //ost2 << datadir << "/livermore/epics2017/pe-ss-cs-" << Z <<".dat";
    ost2 << "../../phot2014/pe-ss-cs-" << Z <<".dat";
    std::ifstream data_file2(ost2.str().c_str());
    
    //IN
    if(data_file2.fail()){
        std::cout << "Error opening data file:  " <<ost2.str().c_str()<<" for sub-shells x-section"  << std::endl;
    }else {
        std::cout << "data file " << ost2.str().c_str() << " is opened" << std::endl;
    }
    std::vector<double> ene[numShells], cs[numShells];
    if(!data_file2) return -1;
    double counter=-1;
    for (int N = 0; N < 99999; N++) {
        //read the file with subshells cross-sections
        // energy || cross-section
        data_file2 >> x >> y;
        if(x < -1.5) { //end of file
            break;
            
        }
        if(x > 0.0){
            counter++;
            ene[nshells].push_back(x);
            cs[nshells].push_back(y);
        }
        
        if(x < 0.0) {
            //it is the end of one shell (ROW -1 -1 reached) ---> write in the file
            nshells++;
        }
    }
    data_file2.close();
    //************
    
    //// GENERATE SUBSHELLS CROSS-SECTIONS
   
    //std::cout<<"emin: "<<emin<<"\temax: "<<emax<<"\nEnPerDecade: "<<nEnPerDecade<<"\ttotEnergyValues: "<<totEnergyValues<<"\tdeltaEn: "<<deltaEn<<std::endl;
    
    char fileName3[512];
    sprintf(fileName3,"denserEpics2014/pe-ss-cs-%d.dat",Z);
    FILE *fp3     = fopen(fileName3,"a");
    std::vector<G4DynamicParticle*> vdp1;
    std::vector<double> subshellsCS;
    
    std::vector<double> allSubshellsCS[numShells]; //allSubshellsCS[i]= cross-sections for shellId i
    std::vector<double> allSubshellsEn[numShells];
    
    double singleCS;
    //for(int iterations=0; iterations<2/*totEnergyValues*/; iterations++){
    //THIS LOOP IS CALCULATING EXACT SS XS VALUES FOR THE SAME ENERGIES STORED in epics2014
//    for(int iterations=0; iterations<numShells; iterations++){
//        for(int sec=0; sec<ene[iterations].size(); sec++)
//        //    for(int sec=0; sec<3; sec++)
//        {
//            //std::cout<<"iteration: "<<sec<<std::endl;
//            double en = ene[iterations][sec]; //std::exp(logEMin+iterations*deltaEn);
//            dParticle.SetKineticEnergy(en);
//            G4double sig4 = livNEW.CrossSectionPerVolume(mat,part,en);
//            livNEW.CalculateSubshellCS(&vdp1,couple,&dParticle,0.0,en, Z, subshellsCS);
//            singleCS=1.e22*subshellsCS[iterations];//because it is retrieved in barns (1e-28*metersˆ2 and meters expressed in millimeters in Geant4 and millimeters=1.-> 1^-28*1^6 = 1^22
//            subshellsCS.clear();
//            //std::cout<<en<<"\t"<<singleCS<<std::endl;
//            allSubshellsCS[iterations].push_back(singleCS);
//            allSubshellsEn[iterations].push_back(en);
//            if(singleCS<=0) std::cout<<"XCAAASSS: "<<singleCS<<"\n";
//            }
//
//    }
    //double logEMax    = std::log(emax);
    int nEnPerDecade=100;
    //I need to keep ony the two first points and then add the rest
    //it needs to be done per shell --> cause the ionization energy is different.
    for(int shell=0; shell<numShells; shell++){
        
        emin=livNEW.GetBindingEnergy(Z, shell);
        double logEMin= std::log(emin);
        
        //this has to be done in logaritmic scale
        int totEnergyValues = nEnPerDecade*std::lrint(std::log10(emax/emin))+1;
        double deltaEn   = std::log(emax/emin)/(totEnergyValues-1.0);
        
        for(int sec=1; sec<totEnergyValues; sec++)
        {
            double en = std::exp(logEMin+sec*deltaEn);
            dParticle.SetKineticEnergy(en);
            G4double sig4 = livNEW.CrossSectionPerVolume(mat,part,en);
            livNEW.CalculateSubshellCS(&vdp1,couple,&dParticle,0.0,en, Z, subshellsCS);
            singleCS=1.e22*subshellsCS[shell];//because it is retrieved in barns (1e-28*metersˆ2 and meters expressed in millimeters in Geant4 and millimeters=1.-> 1^-28*1^6 = 1^22
            if(singleCS<=0) {std::cout<<"!!!! Error in "<<shell<<", for Z:"<<Z<<" at energy: "<<en<<std::endl; exit(-1);}
            subshellsCS.clear();
            //std::cout<<en<<"\t"<<singleCS<<std::endl;
            allSubshellsCS[shell].push_back(singleCS);
            allSubshellsEn[shell].push_back(en);
        }
    }
    //  for (int i=0; i<numShells; i++){
//        for(int j=2; j<allSubshellsEn[i].size(); j++)
//        {
//            std::cout<<"allSubshellsEn["<<i<<"]["<<j<<"]:\t"<<allSubshellsEn[i][j]<<"\tallSubshellsCS["<<i<<"]["<<j<<"]:\t"<<allSubshellsCS[i][j]<<std::endl;
//
//        }
//    }
   
    for (int i=0; i<numShells; i++){
        //the first two element of each shell must be copied from the original file
        fprintf(fp3,"%.8g\t%.8g\n",ene[i][0], cs[i][0]);
        fprintf(fp3,"%.8g\t%.8g\n",ene[i][1], cs[i][1]);
        for(int j=0; j<(int)allSubshellsCS[i].size(); j++){
            if(allSubshellsCS[i][j]<=0)
            {
                std::cout<<"!!!!  Error in "<<i<<", for Z:"<<Z<<" at energy: "<<allSubshellsEn[i][j]<<std::endl; exit(-1);
                
            }
            fprintf(fp3,"%.8g\t%.8g\n",allSubshellsEn[i][j], allSubshellsCS[i][j]);
            
        }
        fprintf(fp3,"-1\t-1\n");
        
    }
    
    fprintf(fp3,"-2\t-0\n");
    fclose(fp3);

    if(verbose > 0) {
        G4cout << "###### End of run # " << G4endl;
    }
    
    delete pFrame;
    delete lFrame;
    delete sFrame;
    partTable->DeleteAllParticles();
    
    G4cout << "###### End of test #####" << G4endl;
}
